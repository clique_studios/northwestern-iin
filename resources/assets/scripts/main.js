/** import external dependencies */
import $ from 'jquery/dist/jquery.js';
import 'jquery-easing';
import 'bootstrap';
import "waypoints/lib/jquery.waypoints.min";
import 'jquery-validation/dist/jquery.validate';
import Swiper from 'swiper/dist/js/swiper';
import "prcomboselect";
import 'animsition/dist/js/animsition.min.js';
import "cssuseragent";
import stickybits from 'stickybits'
import Headroom  from "headroom.js";
import AOS from 'aos';

/** Headroom Init */
var myElement = document.querySelector("nav");
var headroom  = new Headroom(myElement);
headroom.init();

/** Stickybits Init */
stickybits('#stickybit-parent', { useStickyClasses: false, stickyBitStickyOffset: 150 });
stickybits('#stickybit-glossary', { useStickyClasses: false, stickyBitStickyOffset: 0 });
stickybits('.pinned-image img', { useStickyClasses: false, stickyBitStickyOffset: 0 });

// Init scroll animations
AOS.init({
  easing: 'ease-in-out',
	anchorPlacement: 'top-center'
});

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import pageTemplateTemplateHome from './routes/home';
import pageTemplateTemplateAbout from "./routes/about";
import pageTemplateTemplatePeople from "./routes/people";
import pageTemplateTemplateResearch from "./routes/research";
import pageTemplateTemplateNano101 from "./routes/nano101";
import pageTemplateTemplateNanotechnology from "./routes/nanotechnology";
import pageTemplateTemplateNews from "./routes/news";
import pageTemplateTemplateEvents from "./routes/events";

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
	/** All pages */
	common,
	/** Home page */
	pageTemplateTemplateHome,
	pageTemplateTemplateAbout,
	pageTemplateTemplatePeople,
	pageTemplateTemplateResearch,
	pageTemplateTemplateNano101,
	pageTemplateTemplateNanotechnology,
	pageTemplateTemplateNews,
	pageTemplateTemplateEvents
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
