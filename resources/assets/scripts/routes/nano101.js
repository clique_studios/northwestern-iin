import Swiper from "swiper/dist/js/swiper.js";

export default {
  init() {
    // Swiper
    var dateMenu = ['Pre-18th Century', '&nbsp;', '19th Centrury', '&nbsp;', '20th Century', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;']

    var swiperTimeline = new Swiper('.swiper-nano101', {
      slidesPerView: 1,
      loop: true,
      pagination: {
        el: '.timeline-pagination',
        clickable: true,
          renderBullet: function (index, className) {
            return '<span class="' + className + '">' + '<div class="timeline-caption"><p>' + (dateMenu[index]) + '</p></div>' + '</span>';
          },
      },
      navigation: {
        nextEl: '.cs-swiper-button-next',
        prevEl: '.cs-swiper-button-prev',
      },
      a11y: {
        prevSlideMessage: 'Previous slide',
        nextSlideMessage: 'Next slide',
      },
    });

    $(".timeline-pagination p").filter(function(){
        return $(this).html().match(/&nbsp;/) !== null;
    }).addClass("remove-bar");

    // $('.timeline-pagination .swiper-pagination-bullet').click(function(){
    //     $('.timeline-pagination').animate({scrollLeft: leftPos + 200}, 800);
    //     return false;
    // });

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: (target.offset().top - 150)
          }, 1000, "easeInOutExpo");
          return false;
        }
      }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
      $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({target: '#peopleNav',offset: 150});
    $('body').scrollspy({target: '#glossaryNav',offset: 0});
	},
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
