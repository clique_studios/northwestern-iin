export default {
  init() {
  // Nav dropdown on hover
  // if ($(window).width() > 1056) {
  //   function toggleDropdown (e) {
  //     const _d = $(e.target).closest('.dropdown'),
  //     _m = $('.dropdown-menu', _d);
  //     setTimeout(function(){
  //       const shouldOpen = e.type !== 'click' && _d.is(':hover');
  //       _m.toggleClass('show', shouldOpen);
  //       _d.toggleClass('show', shouldOpen);
  //       $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
  //     }, e.type === 'mouseleave' ? 0 : 0);
  //   }
  //   $('body').on('mouseenter mouseleave','.dropdown',toggleDropdown).on('click', '.dropdown-menu a', toggleDropdown);
  // }

  // $(function($) {
  //   if ($(window).width() > 1056) {
  //     $('.navbar .dropdown').hover(function() {
  //       $(this).find('.dropdown-menu').first().stop(true, true).delay(250).searchdeDown();
  //     }, function() {
  //       $(this).find('.dropdown-menu').first().stop(true, true).delay(100).searchdeUp();
  //     });
  //     $('.navbar .dropdown > a').click(function() {
  //       location.href = this.href;
  //     });
  //   }
  // });

  // Hamburger Animation
  $(".hamburger").on("click", function() {
		$(this).toggleClass("is-active");
		if ($(this).hasClass("is-active")) {
			$("body").addClass("menu-open");
		} else {
			$("body").removeClass("menu-open");
		}
	});

  // var inviewWaypoint = $(".row:not(.no-fade)").each(
  //   function() {
  //     $(this).waypoint({
  //       handler: function(direction) {
  //         $(this.element).addClass("loaded");
  //       },
  //     });
  //   }
  // );

  // $('.rule-bottom').waypoint(function(direction) {
  //   if(direction==="down") {
  //     $("#glossaryNav").addClass("loaded");
  //   } else {
  //     $("#glossaryNav").removeClass("loaded");
  //   }
  // }, {
  //   offset: '0%'
  // });

  // Animsition
  $(".animsition").animsition({
    inClass: 'fade-in',
    outClass: 'fade-out',
    inDuration: 1500,
    outDuration: 800,
    linkElement: '.animsition-link',
    loading: true,
    loadingParentElement: 'body',
    loadingClass: 'animsition-loading',
    loadingInner: '',
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    overlay : false,
    overlayClass : 'animsition-overlay-searchde',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });

  // Fix nav top style based on nav height
  var navHeight = $('.navbar').outerHeight();
  $('.dropdown-container').css('top', navHeight+'px');

  // Close nav on window resize
	$(window).resize(function() {
    // Fix nav top style based on nav height
    var navHeight = $('.navbar').outerHeight();
    $('.dropdown-container').css('top', navHeight+'px');
	});

  // Dropdown add class
  $('.dropdown').on('show.bs.dropdown', function () {
    $('body').addClass('open');
  });
  $('.dropdown').on('hide.bs.dropdown', function () {
    $('body').removeClass('open');
  });

	// Form comboSelect
	$("select").comboSelect();

  // About Nav Image Swap
  var $aboutimg = $('.about-default > img'),
  aboutsrc = $aboutimg.attr('src');
  $(".about-dropdown a").on({
    mouseenter: function () {
      var $this = $(this).addClass('hover');
      // Swap attributes
      $aboutimg.data("data-image", $(this).attr("src")).stop(true, true).fadeOut(200, function () {
        $aboutimg.attr('src', $this.data('image'));
      }).fadeIn(200);
    },
    mouseleave: function () {
      $(this).removeClass('hover');
      // Go back to default attribute
      $aboutimg.stop(true, true).fadeOut(200, function () {
        $aboutimg.attr('src', aboutsrc);
      }).fadeIn(200);
    }
  });

  // Education Nav Image Swap
  var $eduimg = $('.education-default > img'),
  edusrc = $eduimg.attr('src');
  $(".education-dropdown a").on({
    mouseenter: function () {
      var $this = $(this).addClass('hover');
      // Swap attributes
      $eduimg.data("data-image", $(this).attr("src")).stop(true, true).fadeOut(200, function () {
        $eduimg.attr('src', $this.data('image'));
      }).fadeIn(200);
    },
    mouseleave: function () {
      $(this).removeClass('hover');
      // Go back to default attribute
      $eduimg.stop(true, true).fadeOut(200, function () {
        $eduimg.attr('src', edusrc);
      }).fadeIn(200);
    }
  });

  // Expanding search bar
  $('.search-button').on('click', function () {
    $('.search-bar').addClass('active');
    $('.navbar-nav').addClass('search-active');
  });
  $('.search-close').on('click', function() {
    $('.search-bar').removeClass('active');
    $('.navbar-nav').removeClass('search-active');
  });


  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
