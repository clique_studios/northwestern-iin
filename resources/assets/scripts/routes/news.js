import jQueryBridget from 'jquery-bridget'
import isotope from 'isotope-layout'

export default {
  init() {
    // init Isotope
    jQueryBridget('isotope', isotope, $);
    var $container = $('.isotope').isotope({
      itemSelector: '.element-item',
      layoutMode: 'vertical',
    });

    // filter functions
    var filterFns = {
      // show if number is greater than 50
      numberGreaterThan50: function() {
        var number = $(this).find('.number').text();
        return parseInt(number, 10) > 50;
      },
      // show if name ends with -ium
      ium: function() {
        var name = $(this).find('.name').text();
        return name.match(/ium$/);
      }
    };

    // bind filter button click
    $('#filters').on('click', 'li', function() {
      var filterValue = $(this).attr('data-filter');
      // use filterFn if matches value
      filterValue = filterFns[filterValue] || filterValue;
      $container.isotope({
        filter: filterValue
      });
    });

    // change is-checked class on buttons
    $('.button-group').each(function(i, buttonGroup) {
      var $buttonGroup = $(buttonGroup);
      $buttonGroup.on('click', 'li', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');
      });
    });

    //****************************
    // Isotope Load more button
    //****************************
    var initShow = 9; //number of items loaded on init & onclick load more button
    var counter = initShow; //counter for load more button
    var iso = $container.data('isotope'); // get Isotope instance

    loadMore(initShow); //execute function onload

    function loadMore(toShow) {
      $container.find(".hidden").removeClass("hidden");

      var hiddenElems = iso.filteredItems.slice(toShow, iso.filteredItems.length).map(function(item) {
        return item.element;
      });
      $(hiddenElems).addClass('hidden');
      $container.isotope('layout');

      //when no more to load, hide show more button
      if (hiddenElems.length == 0) {
        jQuery("#load-more").hide();
      } else {
        jQuery("#load-more").show();
      };

    }

    //append load more button
    $container.after('<div class="row mt-2"><div class="col text-center"><p class="text-primary bold"><a id="load-more">Load More</a></p></div>');

    //when load more button clicked
    $("#load-more").click(function() {
      if ($('#filters').data('clicked')) {
        //when filter button clicked, set initial value for counter
        counter = initShow;
        $('#filters').data('clicked', false);
      } else {
        counter = counter;
      };

      counter = counter + initShow;

      loadMore(counter);
    });

    //when filter button clicked
    $("#filters").click(function() {
      $(this).data('clicked', true);

      loadMore(initShow);
    });

	},
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
