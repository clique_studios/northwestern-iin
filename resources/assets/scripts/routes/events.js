import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

export default {
  init() {

    /** Fullcalendar Init */
    var calendarEl = document.getElementById('calendar');
    var calendar = new Calendar(calendarEl, {
    	plugins: [ dayGridPlugin ],
    	events: [
    		{
    			title: '2019 IIN Symposium: Maps, Materials, and Methods for Navigating the…',
    			start: '2020-03-12',
    			extendedProps: {
    				status: 'done'
    			}
    		},
    		{
    			title: 'IIN Frontiers in Nanotechnology Seminar Series: Natalie Artzi',
    			start: '2020-03-13',
    			backgroundColor: 'Purple',
    			borderColor: 'Purple'
    		}
    	],
    });
    calendar.render();

	},
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
