import ScrollMagic from 'scrollmagic';

export default {
  init() {

  // ScrollMagic controller on Desktop only
  if ($( window ).width() >= 636) {
    // init controller
    var nanoController = new ScrollMagic.Controller();

    // Create pinning scenes dynamically
    $(".pin-scene").each(function() {
      new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0
      })
      .setPin(this)
      .setClassToggle(this, "fade-transition")
      .addTo(nanoController);
    });

    $(".unpin-slide").each(function() {
      // Unpin slide
      new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 1
      })
      .setClassToggle(".pinned-image", "unpin-image")
      .addTo(nanoController);
    });
  }

	},
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
