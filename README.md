# Front-End Boilerplate

---

Last updated 7/29/2016

### Goals and Purpose

1. Looking to unify the tools, resources, and methodologies between the members of our User Interface Engineering team.
2. Speed up initial development with a set of curated tools


## Getting Started

### Before you begin...

Make sure you have the proper build tools and software, as well as the correct version, necessary to use the Front End Boilerplate. The big programs to check are:

```node@>=6.9.0```
```yarn@>=1.6.0```

If node isn't installed, go to the [Node.js Downloads page](https://nodejs.org/en/download/), download it and follow the instructions there.

If yarn isn't installed, go to the [yarn installation docs page](https://yarnpkg.com/en/docs/install#mac-stable) for your platform's installation instructions.

---


### Installation

Begin by cloning the latest version of the repo:

```
git clone https://[USERNAME]@bitbucket.org/clique_studios/front-end-boilerplate.git
```

Unless specified otherwise, git will create a new directory from the clone. Using the command line interface (Terminal, iTerm, Console, PuTTY, etc.) access the newly created directory and install the node packages:

```
cd front-end-boilerplate && yarn install && yarn run build
```
Once that's complete a second script will run automatically, installing all [yarn dependencies](https://bitbucket.org/clique_studios/front-end-boilerplate/src/622fa120da3503b6720fd4097eda0a461c02ed29/bower.json?fileviewer=file-view-default) and compiling the framework, allowing you to dive right into your code.


## Directory Structure

The file structure is as similar to that of [Sage's](https://github.com/roots/sage) as we could get to ensure an easy hand-off from UIE to Back-End, but there are a few areas of difference, pointed out below:

```
project
│   README.md
│   file001.txt
│
└───resources
    │─── assets
         |─── fonts
         |─── scripts
         |─── styles
         ├─── media* - contains video and audio files
         ├─── other* - houses the browserconfig.xml,
                  robots & humans.txt files, as well
                  as other browser-specific files used
                  to register icons and on-screen assets

         └───views* - this directory is home to the pre-compiled HTML
             │      files. These files can, and should, be used as
             │      the templates for modular components that make
             │      up the whole of the site.
             │
             │ *.hbs - All individual files at this level represent
             │          a template to be used in the WordPress theme.
             │
             ├─── footer* - Contains files pertaining to the footer, including
             │              a place to drop in third-party scripts
             │              (Google Analytics, social media widgets, etc.)
             │
             ├─── header* - All header files, with the <head> element seperate
             │              from items like the top navigation and page shell
             │
             └─── modules* - This folder contains templates for any web component
                             that is implemented more than once on the site.
                             Out of the box there is a simple breadcrumbs nav,
                             sample form, and widgets area.

```


## Build Tools

### Yarn

All dependencies are added via Yarn. (Usage: `yarn add package_name`)

To import a script dependency for use acros the site, add an `import` statement to `resources/assets/scripts/main.js`. There you can see `import 'jquery'` imports the files in the `main` specification from the `jquery` package. If you want to override the default includes, or just include an individual file, refer to the way `waypoints` is included by filename. The build tools know to search the `node_modules` folder for files first.

When importing stylesheet dependencies, you can start a file string with a "~" to indicate that it comes from a node package. You can see some examples in `/resources/assets/styles/main.scss`.

To build assets, run `yarn build`. To run a server at http://localhost:8080 and live-compile asests, use `yarn start`.

## Templating

Templates use [Handlebars](https://handlebarsjs.com/) now! Handlebars is syntacticaly similar to Laravel's Blade templating engine, which is used by Sage 9. You can pretty much use regular HTML, with the ability to render javascript variables with `{{ variablename }}` and include other template files like `{{>../path/to/_file}}`.

If you'd like to add additional classes to a body tag, add the `additionalClass="classnames"` attribute to the _head include at the top of the file! This will add whatever classes you enter there (space-separated) to the body classes.

## Creating Styles

Styles are written in SCSS and compiled via Yarn. All SCSS files should be included in main.scss--if you create a new file, add it in the appropriate section to make sure it's compiled along with everything else.

Page-specific styles should go in their own file under the templates folder. To target a specific page, the top-level selector should be `.[the name of the file]`, e.g. `.about`. If you want to create a template for, say, the wordpress single article page, you might want to name it single.html for ease of transferring styles to a Wordpress theme.

If you find yourself re-using a number or color in the styles (like a common set of colors, or a font size), consider adding it to common/_variables.scss to help keep things standardized and make things easier to change globally later.

If you find yourself re-using a particular block of code and styles that's not specific to one page, consider pulling the styles into their own file in the components folder.

## Creating Scripts

The way scripts are set up has changed significantly since Sage 8. While Sage 9 still applies scripting based on the body classes on a page, we split the routes into separate JS files. You'll find a few to start with in `/resources/assets/scripts/routes`.

## Delivery for Production

To deploy a copy of the front-end boilerplate to a server, `git clone` the repository to the `frontend` folder of your project.

## For Back-End Transition

Content for templates resides in `resources/views`. 

## Bug Reporting

Report issues via the Front-End Boilerplate project in [Redmine](https://red.cliquestudios.com/projects/frontend-boilerplate/issues). When possible, include relevant screenshots and operation system/browser version information, and steps that can be taken to reproduce the problem.

## Third-Party Resources

### Currently-Included Libraries

* [animate.css](https://github.com/daneden/animate.css)
* [Flexbox Grid](http://flexboxgrid.com/)
* [Font Awesome](http://fortawesome.github.io/Font-Awesome/icons/)
* [JQuery](http://jquery.com/)
* [JQuery Validation](https://jqueryvalidation.org/)
* [JQuery Easing](https://jqueryui.com/easing/)
* [Swiper](http://idangero.us/swiper/)
* [EasyDropDown](https://github.com/patrickkunka/easydropdown)

### Suggested Additional Libraries

These libraries are not included by default due to the fact that they are not always necessary, but are recommended as solutions to use cases that come up every once in a while.

* (add libraries here)
